# Alias --------------
alias ls="ls -F -h --color=auto --group-directories"
alias grep="grep --color=auto"
alias hgrep="history | grep"
alias ccat="highlight -O ansi --"

# Shortcuts
alias g="git"
alias gap="git add -p"

alias k="kubectl"
alias v="nvim"
alias vi="nvim"
alias r="ranger"
