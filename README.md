# Dotfiles
My Dotfiles, there are many like them but these are mine.

![alt text](./screenshot.png "Dotfiles Screenshot")

# Install

## Requires
- https://github.com/twpayne/chezmoi

## Initialize

```shell
# HTTPS
$ chezmoi init https://codeberg.org/arran/dotfiles.git

# SSH
$ chezmoi init git@codeberg.org:arran/dotfiles.git
```
## Configure

For this configuration to be used across machines, templates are used to fill in user-specific information.

```shell
$ mkdir ~/.config/chezmoi
$ touch ~/.config/chezmoi/chezmoi.toml
```

Inside this `toml` file, fill in the listed fields.

```toml
[data.git]
    email = ""
    name = ""

[data.theme]
    background_image = "~/Pictures/Wallpapers/1.png"

    white = "#ebdbb2"
    dark = "#1d2021"
    
    primary = "#fd472f"
    secondary = "#689d6a"
    hint = "#fabd2f"

# Simple rofi + wl-copy script to automatically copy fields
# listed below. (~/.config/sway/scripts/clips/clips.sh)
# <Name>: <Value>
# Entries must be seperated by a new line.
[data.clips]
    data = """
Email: hello@example.com 

Address: Land, Earth

"""
```

## Apply

Now apply the configuration to create your new dotfiles!

```shell
$ chezmoi apply
```

# Software Used

- sway
- alacritty
- waybar
- mako
- fish
- jq
- wl-clipboard
- playerctl
- rofi
- kpmenu
- tesseract
- grimshot
- neovim
- rofi-emoji
- alacritty
- nwg-bar
- nwg-displays
- pavucontrol
- keepassxc
- wf-recorder
- qcopy
- wpctl (Wireplumber)


# Further Reading

Read more on the [Guide](./GUIDE.md)
